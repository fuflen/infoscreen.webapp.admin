import {ScheduleDTO} from './ScheduleDTO';

export class InfoScreenDTO{

  id : number;
  name : string;
  scheduleList : ScheduleDTO[];
}



