import {MappingDTO} from './MappingDTO';

export class ViewDTO{

  id? : number;
  name : string;
  endpointId : number;
  templateId : number;
  numberOfRows : number;
  refreshDataIntervalInMinutes : number;
  mappingsToBeCreated?: MappingDTO[];
}

