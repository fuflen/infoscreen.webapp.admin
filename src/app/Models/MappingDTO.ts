export class MappingDTO{
  id? : number;
  dynamicFieldName : string;
  externalFieldName : string;
  endpointId : number;
  templateId : number;
}
