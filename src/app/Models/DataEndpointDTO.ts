export class DataEndpointDTO{

  id : number;
  endpointUrl : string;
  DataLocation : string;
  name : string;
}
