export class ScheduleDTO{
  id? : number;
  viewId : number;
  viewName : string;
  onScreenTime? : number;
  infoScreenId : number;
  startDateTime?: Date;
  expireDateTime?: Date;
}
