import { BrowserModule } from '@angular/platform-browser';
import {EventEmitter, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { ToolbarComponent } from './Components/toolbar/toolbar.component';
import {RouterLinkActive, RouterModule, Routes} from '@angular/router';
import { InfoScreenComponent } from './Components/info-screen/info-screen.component';
import { InfoScreenViewComponent } from './Components/info-screen/info-screen-view.component';
import { InfoViewComponent } from './Components/info-view/info-view.component';
import { InfoViewViewComponent } from './Components/info-view/info-view-view.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {
  MAT_DATE_LOCALE,
  MatButtonModule, MatCardModule, MatDatepickerModule,
  MatFormFieldModule, MatGridListModule,
  MatInputModule,
  MatListModule, MatNativeDateModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatStepperModule
} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import { ViewGeneratorComponent } from './Components/view-generator/view-generator.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ViewGeneratorViewComponent } from './Components/view-generator/view-generator-view.component';
import {ViewService} from './Services/ViewServices/view.service';
import {PopulatedViewService} from './Services/ViewServices/populated-view.service';
import {ScheduleService} from './Services/ScreenScheduleServices/schedule.service';
import {InfoScreenService} from './Services/ScreenScheduleServices/info-screen.service';
import { ViewPickerDialogComponent } from './Components/info-view/view-picker-dialog/view-picker-dialog.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {DataEndpointService} from './Services/IntegrationServices/data-endpoint.service';
import {TemplateService} from './Services/TemplateServices/template.service';
import {Mapping} from 'source-map';
import {MappingService} from './Services/MappingServices/mapping.service';

const routes: Routes = [
  {path: '', component: InfoScreenComponent},
  {path: 'InfoViews', component: InfoViewComponent},
  {path: 'ViewGenerator', component: ViewGeneratorComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    InfoScreenComponent,
    InfoScreenViewComponent,
    InfoViewComponent,
    InfoViewViewComponent,
    ViewGeneratorComponent,
    ViewGeneratorViewComponent,
    ViewPickerDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DragDropModule,
    MatSelectModule,
    MatListModule,
    MatStepperModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatCardModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgbModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    RouterModule.forRoot(routes, { useHash: true})
  ],
  providers: [
    ViewService,
    PopulatedViewService,
    ScheduleService,
    InfoScreenService,
    DataEndpointService,
    TemplateService,
    MappingService,
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

