import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Result} from '../../Models/Result';
import {InfoScreenDTO} from '../../Models/InfoScreenDTO';
import {DataEndpointDTO} from '../../Models/DataEndpointDTO';

@Injectable({
  providedIn: 'root'
})
export class DataEndpointService {

  private GatewayAPI : string = environment.GatewayUrl + "IntegrationApi";


  constructor(private httpClient: HttpClient) {
  }

  public GetAllDataEndpoints() : Observable<Result<DataEndpointDTO[]>>
  {
    return this.httpClient.get<Result<DataEndpointDTO[]>>(this.GatewayAPI + "/GetAllEndpoints");
  }

  public CreateDataEndpoint(endpoint : DataEndpointDTO) : Observable<Result<DataEndpointDTO>>
  {
    return this.httpClient.post<Result<DataEndpointDTO>>(this.GatewayAPI + "/CreateEndpoint", endpoint);
  }

  public GetPropertiesFromEndpoint(endpointId : number) : Observable<Result<string[]>>
  {
    return this.httpClient.get<Result<string[]>>(this.GatewayAPI + "/GetPropertiesFromEndpoint/" + endpointId );
  }
}
