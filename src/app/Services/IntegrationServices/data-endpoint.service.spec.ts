import { TestBed } from '@angular/core/testing';

import { DataEndpointService } from './data-endpoint.service';

describe('DataEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataEndpointService = TestBed.get(DataEndpointService);
    expect(service).toBeTruthy();
  });
});
