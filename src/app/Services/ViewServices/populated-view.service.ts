import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Result} from '../../Models/Result';
import {PopulatedViewDTO} from '../../Models/PopulatedViewDTO';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PopulatedViewService {

  private GatewayAPI : string = environment.GatewayUrl + "ViewService";

  constructor(private httpClient: HttpClient) {
  }


  public GetPopulatedView(id : number) : Observable<Result<PopulatedViewDTO>>
  {
    return this.httpClient.get<Result<PopulatedViewDTO>>(this.GatewayAPI + "/GetViewById/" + id);
  }


}
