import { TestBed } from '@angular/core/testing';

import { PopulatedViewService } from './populated-view.service';

describe('PopulatedViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PopulatedViewService = TestBed.get(PopulatedViewService);
    expect(service).toBeTruthy();
  });
});
