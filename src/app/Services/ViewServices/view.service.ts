import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ViewDTO} from '../../Models/ViewDTO';
import {Result} from '../../Models/Result';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ViewService {



  private GatewayAPI : string = environment.GatewayUrl + "ViewService";


  constructor(private httpClient: HttpClient) {
  }



  public GetViews() : Observable<Result<ViewDTO[]>>
  {
    return this.httpClient.get<Result<ViewDTO[]>>(this.GatewayAPI+ "/GetAllViews");
  }

  public GetByEndpointIdAndTemplateId(endpointId : number, templateId : number): Observable<Result<ViewDTO>>
  {
    return this.httpClient.get<Result<ViewDTO>>(this.GatewayAPI+ "/GetByEndpointIdAndTemplateId/" + endpointId + "/" + templateId);
  }

  public CreateView(view : ViewDTO) : Observable<Result<ViewDTO>>
  {
    return this.httpClient.post<Result<ViewDTO>>(this.GatewayAPI+ "/CreateView", view);
  }

  public UpdateView(view : ViewDTO) : Observable<Result<ViewDTO>>
  {
    return this.httpClient.put<Result<ViewDTO>>(this.GatewayAPI+ "/UpdateView", view);
  }

  public DeleteView(viewId : number)
  {
    return this.httpClient.delete<Result<ViewDTO>>(this.GatewayAPI+ "/DeleteView/" + viewId);
  }

}
