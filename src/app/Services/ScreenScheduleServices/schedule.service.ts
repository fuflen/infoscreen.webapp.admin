import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Result} from '../../Models/Result';
import {InfoScreenDTO} from '../../Models/InfoScreenDTO';
import {ScheduleDTO} from '../../Models/ScheduleDTO';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  private GatewayAPI : string = environment.GatewayUrl + "ScreenScheduleApi";

  constructor(private httpClient: HttpClient) {
  }

  public GetAllSchedules() : Observable<Result<ScheduleDTO[]>>
  {
    return this.httpClient.get<Result<ScheduleDTO[]>>(this.GatewayAPI + "/GetAllSchedules");
  }

  public CreateSchedule(schedule : ScheduleDTO)
  {
    return this.httpClient.post<Result<ScheduleDTO>>(this.GatewayAPI + "/CreateSchedule", schedule);//"http://localhost:61911/api/Schedule/Create"
  }

  public DeleteSchedule(scheduleId : number)
  {
    return this.httpClient.delete<Result<ScheduleDTO>>(this.GatewayAPI + "/DeleteSchedule/" + scheduleId);
  }

  UpdateSchedule(schedule: ScheduleDTO) {
    return this.httpClient.put<Result<ScheduleDTO>>(this.GatewayAPI + "/UpdateSchedule", schedule);
  }
}
