import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Result} from '../../Models/Result';
import {PopulatedViewDTO} from '../../Models/PopulatedViewDTO';
import {InfoScreenDTO} from '../../Models/InfoScreenDTO';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InfoScreenService {



  private GatewayAPI : string = environment.GatewayUrl + "ScreenScheduleApi";

  constructor(private httpClient: HttpClient) {
  }

  public GetAllWithScheduleList() : Observable<Result<InfoScreenDTO[]>>
  {
    return this.httpClient.get<Result<InfoScreenDTO[]>>(this.GatewayAPI + "/GetAllInfoScreensWithScheduleList");
  }


  public GetByIdWithScheduleList(id : number) : Observable<Result<InfoScreenDTO>>
  {
    return this.httpClient.get<Result<InfoScreenDTO>>(this.GatewayAPI + "/GetAllInfoScreensWithScheduleList/" + id);
  }


}
