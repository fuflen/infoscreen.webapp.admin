import { TestBed } from '@angular/core/testing';

import { InfoScreenService } from './info-screen.service';

describe('InfoScreenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfoScreenService = TestBed.get(InfoScreenService);
    expect(service).toBeTruthy();
  });
});
