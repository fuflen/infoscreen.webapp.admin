import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Result} from '../../Models/Result';
import {DataEndpointDTO} from '../../Models/DataEndpointDTO';
import {TemplateDTO} from '../../Models/TemplateDTO';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  private GatewayAPI : string = environment.GatewayUrl + "TemplateApi";


  constructor(private httpClient: HttpClient) {
  }

  public GetAllTemplates() : Observable<Result<TemplateDTO[]>>
  {
    return this.httpClient.get<Result<TemplateDTO[]>>(this.GatewayAPI + "/GetAllTemplates");
  }

  public GetTemplate(templateId : number) : Observable<Result<TemplateDTO>>
  {
    return this.httpClient.get<Result<TemplateDTO>>(this.GatewayAPI + "/GetById/" + templateId);
  }

  public CreateTemplate(template : TemplateDTO) : Observable<Result<TemplateDTO[]>>
  {
    return this.httpClient.post<Result<TemplateDTO[]>>(this.GatewayAPI + "/CreateTemplate", template);
  }

  public GetFieldsFromTemplate(templateId : number) : Observable<Result<string[]>>
  {
    return this.httpClient.get<Result<string[]>>(this.GatewayAPI + "/GetFieldsFromTemplate/" + templateId);
  }

  public GetPopulatedTemplate(templateId : number, endpointId) : Observable<Result<TemplateDTO>>
  {
    return this.httpClient.get<Result<TemplateDTO>>(this.GatewayAPI + "/GetPopulatedTemplate/" + templateId + "/" + endpointId + "/" + 10);
  }
}
