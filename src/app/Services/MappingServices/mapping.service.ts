import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Result} from '../../Models/Result';
import {TemplateDTO} from '../../Models/TemplateDTO';
import {MappingDTO} from '../../Models/MappingDTO';

@Injectable({
  providedIn: 'root'
})
export class MappingService {

  private GatewayAPI : string = environment.GatewayUrl + "MappingApi";


  constructor(private httpClient: HttpClient) {
  }

  public GetManyMappings(endpointId : number, templateId: number) : Observable<Result<MappingDTO[]>>
  {
    return this.httpClient.get<Result<MappingDTO[]>>(this.GatewayAPI + "/GetManyMappings/" + endpointId + "/" + templateId);
  }

  public CreateManyMappings(mappings : MappingDTO[]) : Observable<Result<MappingDTO[]>>
  {
    return this.httpClient.post<Result<MappingDTO[]>>(this.GatewayAPI + "/CreateManyMappings", mappings);
  }

  public DeleteManyMappings(endpointId : number, templateId: number) : Observable<Result<MappingDTO[]>>
  {
    return this.httpClient.delete<Result<MappingDTO[]>>(this.GatewayAPI + "/DeleteManyMappings/" + endpointId + "/" + templateId);
  }
}
