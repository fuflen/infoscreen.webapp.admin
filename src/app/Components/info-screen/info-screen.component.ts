import { Component, OnInit } from '@angular/core';
import {InfoScreenService} from '../../Services/ScreenScheduleServices/info-screen.service';
import {InfoScreenDTO} from '../../Models/InfoScreenDTO';

@Component({
  selector: 'app-info-screen',
  templateUrl: './info-screen.component.html',
  styleUrls: ['./info-screen.component.css']
})
export class InfoScreenComponent implements OnInit {

  public infoScreens : InfoScreenDTO[];

  constructor(private infoScreenService : InfoScreenService) { }

  ngOnInit() {
    this.UpdateViewData();
  }

  UpdateViewData() {
    this.infoScreenService.GetAllWithScheduleList().subscribe(result => {this.infoScreens = result.data; console.log(result.data)});
  }
}
