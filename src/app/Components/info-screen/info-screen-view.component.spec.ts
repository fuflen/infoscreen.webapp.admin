import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoScreenViewComponent } from './info-screen-view.component';

describe('InfoScreenViewComponent', () => {
  let component: InfoScreenViewComponent;
  let fixture: ComponentFixture<InfoScreenViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoScreenViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoScreenViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
