import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {InfoScreenDTO} from '../../Models/InfoScreenDTO';

@Component({
  selector: 'app-info-screen-view',
  templateUrl: './info-screen-view.component.html',
  styleUrls: ['./info-screen-view.component.css']
})
export class InfoScreenViewComponent implements OnInit {

  public _infoScreens : InfoScreenDTO[];

  public selectedInfoScreen: InfoScreenDTO  = null;

  @Input()
  set InfoScreens(value : InfoScreenDTO[])
  {
    this._infoScreens = value;
  }

  @Output()
  public updateViewDataEmitter = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public ResetSelectedInfoScreen()
  {
    this.selectedInfoScreen = null;
    this.updateViewDataEmitter.emit();
  }

}
