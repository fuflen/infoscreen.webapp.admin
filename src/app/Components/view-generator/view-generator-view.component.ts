import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ViewDTO} from '../../Models/ViewDTO';
import {TemplateDTO} from '../../Models/TemplateDTO';
import {DataEndpointDTO} from '../../Models/DataEndpointDTO';
import {MappingDTO} from '../../Models/MappingDTO';
import {ViewFieldsModel} from '../../Models/ViewFieldsModel';

@Component({
  selector: 'app-view-generator-view',
  templateUrl: './view-generator-view.component.html',
  styleUrls: ['./view-generator-view.component.css']
})
export class ViewGeneratorViewComponent implements OnInit {

  public viewToCreate : ViewDTO = new ViewDTO();


  public _templates : TemplateDTO[];
  public  _dataEndpoints: DataEndpointDTO[];
  public _templateFields : string[];
  public _endpointFields : string[];

  public loadingData : boolean = false;

  public _generatedViewContent : string;

  public IsUpdating : boolean = false;

  public _viewFieldsModel : ViewFieldsModel;

  public IsEndpointsFethces : boolean = false;


  @Input()
  set SetViewToUpdate(value : ViewDTO)
  {
    console.log("ViewToUpdate updated");
    console.log(value);
    if(value != null) {
      console.log("ViewToUpdate set");
      console.log(value);
      this.viewToCreate = value;
      if (value.id != 0 && value.id != null) {
        alert("A view already exists with the selected Endpoint and Template, you are now editing it. ");
        this.IsUpdating = true;
      } else {
        this.IsUpdating = false;
      }
      console.log(value);

    }
  };


  @Input()
  set templates(value : TemplateDTO[])
  {
    console.log("Templates  updated");
    if(value != null) {
      this._templates = value;
    }
  };

  @Input()
  set dataEndpoints(value : DataEndpointDTO[])
  {
    console.log("Dataendpoints  updated");
    if(value != null) {

      this._dataEndpoints = value;
    }
  };

  @Input()
  set viewFieldModel(value : ViewFieldsModel)
  {
    console.log("Template field updated");
    console.log(value);
    if(value != null) {
      this._endpointFields = value.endpointFields;
      this._templateFields = value.templateFields;
    }
  };

  @Input()
  set endpointFields(value : string[])
  {
    console.log("Endpoint fields  updated");
    if(value != null)
    {
      this.IsEndpointsFethces = true;
    }
  };

  @Input()
  set generatedViewContent(value : string)
  {
    console.log("Generated view content   updated");
    if(value != null) {
      this.loadingData = false;
      this._generatedViewContent = value;
    }
  };

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;


  @Output()
  GetTemplateFieldsEvent = new EventEmitter();

  @Output()
  GetEndpointFieldsEvent = new EventEmitter();

  @Output()
  GenerateViewContentEvent = new EventEmitter();
  @Output()
  CreateViewEvent = new EventEmitter();
  @Output()
  UpdateViewEvent = new EventEmitter();



  constructor(private _formBuilder: FormBuilder) {
    this.viewToCreate.id = 0;
    this.viewToCreate.endpointId = 0;
    this.viewToCreate.templateId = 0;
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  TryGetFieldsFromTemplate()
  {
    let view = {endpointId: this.viewToCreate.endpointId, templateId: this.viewToCreate.templateId};
    this.GetTemplateFieldsEvent.emit(view);
  }

  TryGetFieldsFromEndpoint()
  {
    this.IsEndpointsFethces = false;
    let view = {endpointId: this.viewToCreate.endpointId, templateId: this.viewToCreate.templateId};
    this.GetEndpointFieldsEvent.emit(view);
  }

  TryGenerateViewContent(selectedEndpointId : number, selectedTemplateId : number)
  {
    this.loadingData = true;

    //let view = this.createMappings(selectedEndpointId, selectedTemplateId);
    this.GenerateViewContentEvent.emit(this.viewToCreate);
  }


  createMapping(field: string, property: string) {
    //if(this.viewToCreate.mappingsToBeCreated.filter(x => x.))
    //this.viewToCreate.mappingsToBeCreated.push({templateId: this.viewToCreate.})
  }

  CreateView() {
    this.CreateViewEvent.emit(this.viewToCreate);
  }

  UpdateView() {
    this.UpdateViewEvent.emit(this.viewToCreate);
  }
}

