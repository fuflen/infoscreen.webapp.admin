import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewGeneratorViewComponent } from './view-generator-view.component';

describe('ViewGeneratorViewComponent', () => {
  let component: ViewGeneratorViewComponent;
  let fixture: ComponentFixture<ViewGeneratorViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGeneratorViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGeneratorViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
