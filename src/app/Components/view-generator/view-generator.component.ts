import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataEndpointService} from '../../Services/IntegrationServices/data-endpoint.service';
import {DataEndpointDTO} from '../../Models/DataEndpointDTO';
import {TemplateDTO} from '../../Models/TemplateDTO';
import {TemplateService} from '../../Services/TemplateServices/template.service';
import {ViewService} from '../../Services/ViewServices/view.service';
import {ViewDTO} from '../../Models/ViewDTO';
import {MappingService} from '../../Services/MappingServices/mapping.service';
import {ViewFieldsModel} from '../../Models/ViewFieldsModel';
import {MappingDTO} from '../../Models/MappingDTO';

@Component({
  selector: 'app-view-generator',
  templateUrl: './view-generator.component.html',
  styleUrls: ['./view-generator.component.css']
})
export class ViewGeneratorComponent implements OnInit {

  public dataEndpoints : DataEndpointDTO[];
  public templates : TemplateDTO[];
  //public templateFields : string[];
  public endpointFields : string[];
  public generatedViewContent : string;
  public isUpdating : boolean;

  public viewToUpdate : ViewDTO;

  public _viewFieldsModel : ViewFieldsModel;

  constructor(private endpointService : DataEndpointService, private templateService : TemplateService, private viewService : ViewService, private mappingService : MappingService) {
    //endpointService.CreateDataEndpoint({id: 2312, endpointUrl: "http://www.someurl.com", name: "SomeEndpoint"}).subscribe(x => console.log(x));
    endpointService.GetAllDataEndpoints().subscribe(result => {console.log(this.dataEndpoints = result.data); this.dataEndpoints = result.data;});

    templateService.GetAllTemplates().subscribe(result => {this.templates = result.data; console.log(this.templates);});

        //viewService.CreaetView({id: 5678, name: "NewViewInTown", endpointId: this.dataEndpoints[0].id, templateId: this.templates[0].id, numberOfRows: 10, refreshDataIntervalInMinutes: 5}).subscribe(result => console.log(result));
      //});
    //});


  }

  ngOnInit() {

  }


  GetFieldsFromTemplate(view : ViewDTO)
  {

    if(view.templateId != null) {
      this.templateService.GetFieldsFromTemplate(view.templateId).subscribe(result => {
        this._viewFieldsModel = {templateFields: result.data, endpointFields: this.endpointFields};

      if (view.endpointId != null) {

        this.viewService.GetByEndpointIdAndTemplateId(view.endpointId, view.templateId).subscribe(result => {
          if (result.data != null) {
            let view = result.data;
            this.mappingService.GetManyMappings(view.endpointId, view.templateId).subscribe(result => {
              view.mappingsToBeCreated = result.data;
              console.log(result);
              if (result.data != null) {
                this.GetPopulatedTemplate(view);
                this.viewToUpdate = view;

                }
            });
          } else
            {

              view.mappingsToBeCreated = this.createMappingsForView(view);
              this.viewToUpdate = view;
              console.log(view);

              this.GetEmptyTemplate(view.templateId);

            }
        });
      } else {

        this.GetEmptyTemplate(view.templateId);
        }
    });
    }
  }

  ResetViewToUpdate()
  {
    this.viewToUpdate.id = 0;
    this.viewToUpdate.name = "";
    this.viewToUpdate.refreshDataIntervalInMinutes = 0;
    this.viewToUpdate.numberOfRows = 0;
  }

  GetFieldsFromEndpoint(view : ViewDTO)
  {
    console.log(view.endpointId);
    this.endpointService.GetPropertiesFromEndpoint(view.endpointId).subscribe(result => {this.endpointFields = result.data; console.log(result)});
  }

  GetPopulatedTemplate(view : ViewDTO)
  {
    this.generatedViewContent = null;
    this.templateService.GetPopulatedTemplate(view.templateId, view.endpointId).subscribe(result => {

      this.generatedViewContent = result.data.htmlContent;
      console.log(result.data.htmlContent)});
  }

  GetEmptyTemplate(templateId : number)
  {
    this.templateService.GetTemplate(templateId).subscribe(result => this.generatedViewContent = result.data.htmlContent);
  }

  GetGeneratedViewContent(view : ViewDTO)
  {
    console.log(view);
    this.mappingService.GetManyMappings(view.endpointId, view.templateId).subscribe(result => {
      if(result.data != null && result.data != [])
      {
        console.log(result);
        this.mappingService.DeleteManyMappings(view.endpointId, view.templateId).subscribe(result => {
          console.log("Deleted");
          console.log(result);
          this.mappingService.CreateManyMappings(view.mappingsToBeCreated).subscribe(result => {
            console.log("Created");
            console.log(result);
            this.GetPopulatedTemplate(view);
          });
        });
      }
    });
  }

  CreateView(view : ViewDTO)
  {
    console.log(view);
    this.viewService.CreateView(view).subscribe(result => {console.log(result); alert("View Created")});
    //this.viewService.CreateView(view).subscribe(result => {
    //  console.log(result);
    //})
  }

  UpdateView(view : ViewDTO)
  {
    console.log(view);
    this.viewService.UpdateView(view).subscribe(result => {console.log(result); alert("View Updated")});
  }

  createMappingsForView(view : ViewDTO )
  {

    console.log("Creating mapping");
    console.log(this._viewFieldsModel.endpointFields);
    console.log(this._viewFieldsModel.templateFields);
    let mappings = [];

    this._viewFieldsModel.templateFields.forEach(field => {
      let mapping : MappingDTO = {
        dynamicFieldName: field,
        externalFieldName: this._viewFieldsModel.endpointFields[this._viewFieldsModel.templateFields.indexOf(field)],
        endpointId: view.endpointId,
        templateId: view.templateId
      };
      mappings.push(mapping);
    });
    console.log(mappings);

    return mappings;
  }

}

