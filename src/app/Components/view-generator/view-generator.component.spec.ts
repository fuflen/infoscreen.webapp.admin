import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewGeneratorComponent } from './view-generator.component';

describe('ViewGeneratorComponent', () => {
  let component: ViewGeneratorComponent;
  let fixture: ComponentFixture<ViewGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
