import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {ViewDTO} from '../../Models/ViewDTO';
import {InfoScreenDTO} from '../../Models/InfoScreenDTO';
import {ScheduleDTO} from '../../Models/ScheduleDTO';
import {ViewPickerDialogComponent} from './view-picker-dialog/view-picker-dialog.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-info-view-view',
  templateUrl: './info-view-view.component.html',
  styleUrls: ['./info-view-view.component.css']
})
export class InfoViewViewComponent implements OnInit {

  @Input()
  selectedInfoScreen : InfoScreenDTO;



  @Output()
  public GetPopulatedViewEmitter = new EventEmitter();
  @Output()
  public goBackEmitter = new EventEmitter();

  @Output()
  public populateViewList = new EventEmitter();

  @Output()
  public CreateScheduleEvent = new EventEmitter();
  @Output()
  public UpdateScheduleEvent = new EventEmitter();

  @Output()
  public DeleteScheduleEmitter = new EventEmitter();



  public _views : ViewDTO[] = [];

  public loadingData : boolean = false;
  public selectedViewId : number;
  public _selectedViewContent : string;


  public scheduleToEdit : ScheduleDTO;

  public _schedules: ScheduleDTO[] = [];

  public _pickedViewToAdd : ViewDTO;

  private addingNewView : boolean = false;


  @Input()
  set selectedViewContent(value : string)
  {
    this._selectedViewContent = value;
    if(value.length > 1)
    this.loadingData = false;
  }

  @Input()
  set views(value)
  {
    this._views = Object.assign(value);
  }

  @Input()
  set schedules(value)
  {
    this._schedules = Object.assign(value);
  }

  constructor() { }

  ngOnInit() {
  }



  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this._schedules, event.previousIndex, event.currentIndex);
    console.log(this._schedules[event.previousIndex]);
  // Update view position here!

}



  selectView(id : number)
  {
    this.loadingData = true;
    this.selectedViewId = id;
    this.GetPopulatedViewEmitter.emit(id);
  }

  GoBack()
  {
    this.goBackEmitter.emit();
  }

  openDialog(): void {
    this.populateViewList.emit();
    this.addingNewView = true;
  }

  newCreatedSchedule(schedule : ScheduleDTO)
  {
    console.log(schedule);
    this.CreateScheduleEvent.emit(schedule);
  }

  newUpdatedSchedule(schedule : ScheduleDTO)
  {
    console.log(schedule);
    this.UpdateScheduleEvent.emit(schedule);
  }

  DeleteSchedule(id: number) {
    if(confirm("Are u sure u want to delete schedule?")) {
      this.DeleteScheduleEmitter.emit(id);
    }
  }

  EditSchedule(schedule: ScheduleDTO) {
    console.log(schedule);
    this.scheduleToEdit = schedule;
    this.populateViewList.emit();
    this.addingNewView = true;
  }

  resetScheduleToEdit()
  {
    this.scheduleToEdit = null;
  }
}


