import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoViewViewComponent } from './info-view-view.component';

describe('InfoViewViewComponent', () => {
  let component: InfoViewViewComponent;
  let fixture: ComponentFixture<InfoViewViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoViewViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoViewViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
