import {AfterContentInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewService} from '../../Services/ViewServices/view.service';
import {ViewDTO} from '../../Models/ViewDTO';
import {PopulatedViewService} from '../../Services/ViewServices/populated-view.service';
import {InfoScreenDTO} from '../../Models/InfoScreenDTO';
import {ScheduleDTO} from '../../Models/ScheduleDTO';
import {ScheduleService} from '../../Services/ScreenScheduleServices/schedule.service';
import {InfoScreenService} from '../../Services/ScreenScheduleServices/info-screen.service';

@Component({
  selector: 'app-info-view',
  templateUrl: './info-view.component.html',
  styleUrls: ['./info-view.component.css']
})
export class InfoViewComponent implements OnInit, AfterContentInit {



  private views : ViewDTO[] = [];
  private schedules: ScheduleDTO[] = [];

  public _selectedInfoScreen : InfoScreenDTO = null;

  @Output()
  public goBackEmitter = new EventEmitter();

  @Input()
  set selectedInfoScreen(value : InfoScreenDTO)
  {
    this._selectedInfoScreen = value;
  }


  public selectedViewContent : string = "";




  constructor(private viewService : ViewService, private populatedViewService : PopulatedViewService, private scheduleService : ScheduleService, private infoScreenService : InfoScreenService) {



  }

  ngOnInit() {

  }

  ngAfterContentInit(): void {
    if(this._selectedInfoScreen != null)
    {
      this.schedules = this._selectedInfoScreen.scheduleList;
    }
    else{
      this.viewService.GetViews().subscribe(result => {this.views = result.data; console.log(result.data)});
    }
  }

  setViewList()
  {
    this.viewService.GetViews().subscribe(result => {this.views = result.data; console.log(result.data)});
  }

  GetPopulatedView(id : number)
  {
    console.log(id);
    this.selectedViewContent = "";
    this.populatedViewService.GetPopulatedView(id).subscribe(result => {console.log(result); this.selectedViewContent = result.data.viewContent});
  }

  GoBack()
  {
      this.goBackEmitter.emit();
  }

  AddScheduleToInfoScreen(schedule : ScheduleDTO)
  {
    let view = this.views.filter(x => x.id == schedule.viewId)[0];
    console.log(view);
    schedule.viewName = view.name;
    schedule.infoScreenId = this._selectedInfoScreen.id;

    console.log(schedule);
    this.scheduleService.CreateSchedule(schedule).subscribe(result =>
    {
      console.log(result);
      this.infoScreenService.GetByIdWithScheduleList(this._selectedInfoScreen.id).subscribe(result => this.schedules = result.data.scheduleList)
    });
  }

  UpdateSchedule(schedule : ScheduleDTO)
  {
    console.log("WER ARE HERERERERE");
    console.log(schedule);
    this.scheduleService.UpdateSchedule(schedule).subscribe(result =>
    {
      console.log(result);
      this.infoScreenService.GetByIdWithScheduleList(this._selectedInfoScreen.id).subscribe(result => this.schedules = result.data.scheduleList);
    });
  }

  DeleteSchedule(id: number) {
    this.scheduleService.DeleteSchedule(id).subscribe(result => {console.log("Deleted schedule: " + result.data.id);

      this.infoScreenService.GetByIdWithScheduleList(this._selectedInfoScreen.id).subscribe(result => this.schedules = result.data.scheduleList)});
  }
}
