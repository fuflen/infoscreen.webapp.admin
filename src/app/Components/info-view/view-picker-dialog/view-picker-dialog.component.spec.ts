import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPickerDialogComponent } from './view-picker-dialog.component';

describe('ViewPickerDialogComponent', () => {
  let component: ViewPickerDialogComponent;
  let fixture: ComponentFixture<ViewPickerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPickerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPickerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
