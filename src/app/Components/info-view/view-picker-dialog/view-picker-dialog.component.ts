import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewDTO} from '../../../Models/ViewDTO';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {ScheduleDTO} from '../../../Models/ScheduleDTO';

@Component({
  selector: 'app-view-picker-dialog',
  templateUrl: './view-picker-dialog.component.html',
  styleUrls: ['./view-picker-dialog.component.css']
})
export class ViewPickerDialogComponent implements OnInit {

  _views : ViewDTO[];
  scheduleToCreate : ScheduleDTO = new ScheduleDTO();
  isEditing : boolean = false;

  today : Date = new Date();

  private defaultDateTime = "0001-01-01T00:00:00";

  @Input()
  set views(value : ViewDTO[])
  {
    this._views = value;
    console.log(value);
  }

  @Input()
  set setScheduleToEdit(value : ScheduleDTO)
  {
    console.log(value);
    if(value != null) {
      console.log(value);
      this.isEditing = true;
      console.log(value.startDateTime + "");

      value.startDateTime = value.startDateTime+"" != this.defaultDateTime ? value.startDateTime : null;
      value.expireDateTime = value.expireDateTime+"" != this.defaultDateTime ? value.expireDateTime : null;

      this.scheduleToCreate = value;
      console.log(value);
    }
  }

  @Output()
  newCreatedScheduleEvent = new EventEmitter();

  @Output()
  newUpdatedScheduleEvent = new EventEmitter();

  @Output()
  resetScheduleToEdit = new EventEmitter();

  constructor() {
    this.scheduleToCreate.viewId = 0;
  }

  onNoClick(): void {
  }

  ngOnInit(): void {
  }

  Save()
  {
      this.newCreatedScheduleEvent.emit(this.scheduleToCreate);
      this.resetModal();
  }

  Edit()
  {
    this.newUpdatedScheduleEvent.emit(this.scheduleToCreate);
    this.resetModal();
  }

  isNumberKey(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }


  resetModal() {
    this.scheduleToCreate = new ScheduleDTO();
    this.scheduleToCreate.viewId = 0;
    this.isEditing = false;
    this.resetScheduleToEdit.emit();
  }
}
